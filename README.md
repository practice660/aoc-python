# Description

This is a simple collection of code challenges completed using Python as presented by \
[AOC](https://adventofcode.com)