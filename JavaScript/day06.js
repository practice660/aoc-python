let day06 = function() {

// the commented code from below does the same thing as this more concise version
let verifyNoDuplicates = array => array.filter((item, index) => array.indexOf(item) !== index);

// let verifyNoDuplicates = function(array) {
//     return array.filter(function(curVal, i) {
//         return array.indexOf(curVal) !== i;
//         // this works because indexOf finds the first occurance of a value in an array
//         // if the index of the item does not match then a duplicate exists before the index of 
//         // the current value.
//     });
// };

function findMarker(input, streamLength) {
    let index = 1;
    let received = [];
    for(let char of input) {
        received.push(char);
        if (received.length === streamLength &&
            verifyNoDuplicates(received).length === 0) {
            return index;
        } else if (received.length === streamLength){
            received.shift();
        }
        index++;
    }
}

let getAnswer = function(input) {

    console.log(`The start of packet marker is: ${findMarker(input, 4)}`);
    console.log(`The start of message marker is: ${findMarker(input, 14)}`);

};

    return {
        getAnswer: getAnswer
    };
};

module.exports = day06();