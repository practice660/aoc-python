const day03 = function() {
    'use strict';

    /*
    Formats input as per specification of the challenge. Each line
    of input must be split in half to eventually be compared with 
    one another to find a common item existing in each half

    returns multidimensional array representing both halves of each
    line of input.
    */
    function formatInput(input){
        let pivot;
        let comp1;
        let comp2;
        let compartments = [];
        let ruckSacks = [];
        for(let line of input) {
            pivot = line.length / 2;
            comp1 = line.slice(0, pivot);
            comp2 = line.slice(pivot);
            compartments.push(comp1, comp2);
            ruckSacks.push(compartments);
            compartments = [];
        }
        return ruckSacks;
    }

    function spliceIntoChunks(arr, chunkSize){
        const res = [];
        while (arr.length > 0) {
            const chunk = arr.splice(0, chunkSize);
            res.push(chunk);
        }
        return res;
    }

    function findCommonBadge(arr){
        
    }

    // utility function to determine if a char is uppercase
    function isUpperCase(string){
        return string.toUpperCase() === string;
    }

    // return the value of the character as specified in the challenge
    function convertCharValue(char){
        if(isUpperCase(char)){
            return char.charCodeAt(0) - 38;
        } else {
            return char.charCodeAt(0) - 96;
        }
    }

    let processPartOne = function (input){
        let total = 0;
        for(let sack of formatInput(input)){
            for(let item of sack[0]){
                if(sack[1].includes(item)){
                    // console.log(`similar item is: ${item} and it's value is ${convertCharValue(item)}`);
                    // console.log(`total is: ${total}`);
                    total = total + convertCharValue(item);
                    // console.log(`new total is: ${total}`);
                    break;
                }
            }
        }
        return total;
    };

    let processPartTwo = function(input) { 
        let groups = spliceIntoChunks(input,3);
        let total = 0;
        for(let group of groups){
            for(let char of group[0]){
                if(group[1].includes(char) && group[2].includes(char)){
                    console.log(`Badge is: ${char}`);
                    total = total + convertCharValue(char);
                    break;
                }
            }
        }
        return total;
        
        
    };

    //public interface
    return {
        processPartOne: processPartOne,
        processPartTwo: processPartTwo
    };

};

module.exports = day03();

