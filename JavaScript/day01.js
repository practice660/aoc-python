const fs = require('fs');

let lines = fs.readFileSync('../Input/day01.txt').toString().replace(/\r\n/g, " ").split(" ");


var total = 0;
var totals = [];

for(let line of lines) {
    if(isNaN(parseInt(line))) {  
        totals.push(total);
        total = 0;
    } else {
        total = total + parseInt(line);
    }
}

totals.sort(function(a, b) {
    return a - b;
});

for(let i = 3; i > 0; i--){
    total += totals.pop();
}

console.log(total);

