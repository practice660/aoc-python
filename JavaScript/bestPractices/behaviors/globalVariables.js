
var toPrint = "print me";

function print(out){
    'use strict';
    // use this inside function 
    // var stringToPrint = out;
    // not declaring variable will push this variable to global scope
    stringToPrint = out; 
    console.log(stringToPrint);
    console.log(toPrint);
}

// console.log(stringToPrint);
print('Hello');
console.log(stringToPrint);