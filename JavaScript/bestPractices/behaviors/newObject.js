(function(){
    'use strict';
    var obj = function(){
        let _this = this; // hold on to this scope within the function
        // take a copy of "this" and use it to reference "this"
        this.hello = 'hello';
        this.greet = function(){
            console.log(_this.hello);
        };
    
        this.delayGreeting = function(){
            // setTimeout(_this.greet.bind(this), 1000); one alternative but not best practice
            setTimeout(_this.greet, 1000);
        };
    };
    
    var greeter = new obj(); // new this scope created
    // greeter.greet();
    greeter.delayGreeting(); // get undefined
}());

