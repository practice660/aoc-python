//variables first
let x = 10;
//functions next
function print(input){
    //variables first
    let x = 0;
    //functions next
    function log(){
        //log stuff
    }
    // run code
    console.log(input);
}
// run code
print(10);

// follow this pattern every time a new scope is created