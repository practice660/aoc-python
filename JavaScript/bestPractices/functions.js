expression();
myFunc();


// Why do this?
// can cause problems (undefined due to hoisting)
var expression = function(){
    console.log('hi from my expression');
};

function myFunc() {
    console.log('hi from my func');
}