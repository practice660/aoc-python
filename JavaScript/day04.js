const day04 = function(){
'use strict';

    let startA;
    let endA;
    let startB;
    let endB;

    function getRangePairs(input){
        let rangePairs = [];
        for(let item of input){
            rangePairs.push(item.split(','));
        }
        return rangePairs;
    }

    function setStartEndVals(r1, r2) {
        let range1 = r1.split('-');
        let range2 = r2.split('-');
        startA = parseInt(range1[0]);
        endA = parseInt(range1[1]);
        startB = parseInt(range2[0]);
        endB = parseInt(range2[1]);
    }

    function rangeFullyContained() {   
        console.log(`${startA} ${endA} >> ${startB} ${endB}`);
        if(startA >= startB && endA <= endB ||
            startA <= startB && endA >= endB){
                return true;
            }
        return false;
    }

    function rangeOverlapping() {
        console.log(`${startA} ${endA} >> ${startB} ${endB}`);
        if( startB <= startA  &&
            endB >= startA ||
            startB >= startA    &&
            endA >= startB ) {
                return true;
        } 
        return false;
    }

    let countOverlapping = function(input) {
        let rangePairs = getRangePairs(input);
        let count = 0;
        for(let pair of rangePairs) {
            setStartEndVals(pair[0], pair[1]);
            // console.log(`Range ${pair[0]} is being compared to ${pair[1]}`);
            if(rangeOverlapping()) {
                console.log("pairs are overlapping");
                count++;
            }
        }
        return count;
    };

    let countFullyContained = function(input) {
        let rangePairs = getRangePairs(input);
        let count = 0;
        for(let pair of rangePairs) {
            setStartEndVals(pair[0], pair[1]);
            // console.log(`Range ${pair[0]} is being compared to ${pair[1]}`);
            if(rangeFullyContained()) {
                console.log("one pair is contained in the other");
                count++;
            }
        }
        return count;
    };

    return{
        countFullyContained: countFullyContained,
        countOverlapping: countOverlapping
    };
};

module.exports = day04();
