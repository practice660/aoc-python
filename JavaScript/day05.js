const day05 = function(){

    // represents each stack of crates
    const stacks = {
        1: [],
        2: [],
        3: [],
        4: [],
        5: [],
        6: [],
        7: [],
        8: [],
        9: []
    };

    // object that contains all movement data
    // moves: the list of move instructions
    // amount: the amount of crates to be moved
    // src: the source from which crates are to be moved
    // dest: the destination to which crates will be moved.
    const movement = {
        moves: [],
        amount: 0,
        src:    "",
        dest:   ""
    };

    // extracts movement data strings representing each move (rest of data after line 9)
    function setMoves(input) {
        movement.moves = input.slice(10);
        // console.log(movement.moves);
    }

    // initializes the stacks object with data from the input (first 8 lines of the data)
    function initializeStacks (input) {
        let index;
        let start;
        let stack;
        for(let line of input.slice(0,9)) {
            start = 0;
            for(let char of line) {
                if(char.match(/[a-zA-Z]/)) {
                   index = line.indexOf(char, start);
                   stack = input[8][index];
                   stacks[stack].push(char);
                   start = index + 1;
                }
            }
            
        }
        //console.log(stacks);
    }

    
    function moveCrates() {
        // get the stack of crates to be moved
        let cratesMoved = stacks[movement.src].slice(0, movement.amount);

        // move the crates in reverser order to simulate moving them all at once.
        for(let i = 1; i <= movement.amount; i++) {
            crate = cratesMoved.pop();
            console.log(`Moving ${crate} from stack ${movement.src} to ${movement.dest}`);
            stacks[movement.dest].unshift(crate); // part II solution
        }

        // clear the source stack of the crates moved. slice does not achieve this.
        for(let i = 1; i <= movement.amount; i++) {
            stacks[movement.src].shift();
        }

        console.log(`new source stack ${stacks[movement.src]} new dest stack ${stacks[movement.dest]}`);
    }
    /*
    Handles processing of moving crates from one stack to another.
    Uses parseMove to extract movement data from string
    */
    function processMoves(){
        for(let move of movement.moves) {
            parseMove(move); // sets values for this move
            console.log(`Move is ${move}`);
            moveCrates();
        }
        console.log(stacks);
    }

    /*
    Utility method to parse a string and get details of the move
    amount: to be moved
    src: from what stack
    dest: to what stack
    sets these details on the movement object.
    */
    function parseMove(move) {
        let formatMove = move.replace(/\D/g, ' ').trim().replace(/\s+/g, ':').split(":");
        movement.amount = parseInt(formatMove[0]);
        movement.src = formatMove[1];
        movement.dest = formatMove[2];
    }

    /*
    This is the main public interface. It utilizes day05 utility methods
    initializeStacks,
    setMoves,
    processMoves

    Returns an array of crates found at the top of each stack of crates
    */
    let getAnswer = function(input) {
        initializeStacks(input);
        setMoves(input);
        processMoves();
        let processStacks = Object.keys(stacks);
        let answer = [];
        for(let stack of processStacks) {
            answer.push(stacks[stack][0]);
        }
        // console.log(stacks);
        console.log(answer);
    }; 
    
    return {
        getAnswer: getAnswer
    };
};



module.exports = day05();