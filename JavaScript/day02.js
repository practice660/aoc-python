const fs = require('fs');

let lines = fs.readFileSync('../Input/day02.txt').toString().trim().replace(/\r\n/g, ":").replace(/\s/g, "").split(":");


// opponent             me
// A         rock       X 
// B         paper      Y
// C         scissors   Z

// Win       Loss       Draw
// 6           0        3
const choice = {
    rock: 'A',
    paper: 'B',
    scissors: 'C'
};

const myChoice = {
    rock: 'X',
    paper: 'Y',
    scissors: 'Z'
};

const choiceBonus = {
    rock: 1,
    paper: 2,
    scissors: 3
};

const winCond = {
    win: 'Z',
    lose: 'X',
    draw: 'Y'
};

const win = {
    rock: 'paper',
    paper: 'scissors',
    scissors: 'rock'
};

const lose = {
    rock: 'scissors',
    paper: 'rock',
    scissors: 'paper'
};

//******* Part I solution ************ */
// let score = 0;

// for(let line of lines) {
//     var op = decryptKey(choice, line[0]);
//     var me = decryptKey(myChoice, line[1]);
//     var bonus = 0;
//     if(op == me) {
//         bonus = 3;
//     } else if ( op == 'rock' && me == 'paper' ||
//                 op == 'paper' && me == 'scissors' ||
//                 op == 'scissors' && me == 'rock') {
//                     bonus = 6;
//     } else {
//         bonus = 0;
//     }
//     score += bonus + choiceBonus[me];
// }
// console.log(score);

/********** Part II solution *******/
let score = 0;

for(let line of lines) {
    var op = decryptKey(choice, line[0]);
    var me;
    var bonus = 0;
    switch(decryptKey(winCond, line[1])) {
        case 'win':
            me = win[op];
            bonus = 6;
            break;
        case 'lose':
            bonus = 0;
            me = lose[op];
            break;
        case 'draw':
            me = op;
            bonus = 3;
            break;
    }
    score += bonus + choiceBonus[me];
}
console.log(score);

function decryptKey(obj, value) {
   for(let key of Object.keys(obj)) {
        if(obj[key] === value) {
            return key;
        }
   }
}


