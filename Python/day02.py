from pathlib import Path
from enum import Enum
# get the path to the file to open
p = Path(__file__).with_name('day02.txt')

# open the file to read
input = p.open('r')

# Read the lines
lines = input.readlines()


# A = Rock
# B = Paper
# C = Scissors

# X = Rock
# Y = Paper
# Z = Scissors

# Scoring per round
# Rock = 1
# Paper = 2
# Scissors = 3
# + 0 (win) + 3 (draw) + 6 (win)

# Part II 
# Y = Draw
# X = Lose
# Z = Win

class Win(Enum):
    A = 'Y'
    B = 'Z'
    C = 'X'

class Lose(Enum):
    A = 'Z'
    B = 'X'
    C = 'Y'

class Draw(Enum):
    A = 'X'
    B = 'Y'
    C = 'Z'

class Score(Enum):
    X = 1
    Y = 2
    Z = 3

def calculateScore(op, me):
    bonus = 0
    if me == 'Z': # I am supposed to win
        print("Win")
        me = Win[op].value
        bonus = 6
    elif me == 'X': # I am supposed to lose
        print("lose")
        me = Lose[op].value
    else: # I am supposed to tie
        print("tie")
        me = Draw[op].value
        bonus = 3
    print("opponent guessed " + line[0] + " You guessed " + line[2])
    print("adding " + str(Score[me].value) + " to " + str(bonus))
    return Score[me].value + bonus

totalScore = 0

for line in lines:
    totalScore += calculateScore(line[0], line[2])
print(totalScore)

