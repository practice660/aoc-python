from pathlib import Path

# get the path to the file to open
p = Path(__file__).with_name('day09.txt')

# open the file to read
input = p.open('r')
# Read the lines
lines = input.readlines()

# starting point
S = 0
# head position
H = 0
# tail position
T = 0

for line in lines:
    mv = line.split()
    print(line)
    print("moving " + mv[0] + " " + str(mv[1]) + " spaces")




