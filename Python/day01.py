from pathlib import Path
import numpy as np

# get the path to the file to open
p = Path(__file__).with_name('day01.txt')

# open the file to read
input = p.open('r')
# Read the lines
lines = input.readlines()
#create array to hold values for each elf
elves = []
elf = []
elfTotals = []
index = 0

# creating array of elves with array of calories to be added together
for line in lines:
    if line == "\n":
        elves.append(elf)
        elf = []
        index += 1
        continue
    else:
         elf.append(line.strip())

for elf in elves:
    itemCalCount = 0 
    for item in elf:
        itemCalCount = itemCalCount + int(item)
    
    elfTotals.append(itemCalCount)

arrSort = np.sort(elfTotals)
li = len(arrSort)
print(arrSort[li-1] + arrSort[li-2] + arrSort[li-3])


        
        