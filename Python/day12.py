from pathlib import Path

class Path:
    def __init__(self, x, y, distance):
        self.x = x
        self.y = y
        self.distance = distance

paths = []
# get the path to the file to open
p = Path(__file__).with_name('day12.txt')


# open the file to read
input = p.open('r')

# Read the lines
lines = input.readlines()

# array containing rows of characters
rows = []

# add each array of characters to each respective row
for line in lines:
    rows.append(list(line.strip()))



cRow = 0 # current Row
cChar = 0 # current character position in Row
cDistance = 0
dRow = 0 # destination Row
dChar = 0 # destination character position in Row

rowIndex = 0

for row in rows:
    for char in row:
        if char == 'S':
            cRow = rowIndex
            cChar= row.index('S')
        elif char == 'E':
            dRow = rowIndex
            dChar = row.index('E')
    rowIndex += 1




