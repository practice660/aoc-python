from pathlib import Path

# get the path to the file to open
p = Path(__file__).with_name('day12.txt')


# open the file to read
input = p.open('r')

# Read the lines
lines = input.readlines()

grid = []
source = (0,0,0) # x, y, Distance from itself
destination = (0,0) # x, y

for line in lines:
    grid.append(list(line.strip()))

rowIndex = 0
for y in grid:
    for x in y:
        if x == 'S':
            source[0] = x.index('S')
            source[1] = rowIndex
        elif x == 'E':
            destination[0] = x.index('S')
            destination[1] = rowIndex
    rowIndex += 1
x = source[0]
y = source[1]
print(grid([x][y]))
