from pathlib import Path

# get the path to the file to open
p = Path(__file__).with_name('day03.txt')

# open the file to read
input = p.open('r')
# Read the lines
lines = input.readlines()

score = 0
sack = []

for line in lines:
   l = line.strip('\n')
   pivot = int(len(l) / 2)
   sack = [l[:pivot], l[pivot:]]
   print(sack)
   for item in sack[0]:
    if item in sack[1]:
        # same item in both sides
        print(item)
        if item.isupper():
            score += ord(item) - 38
        else:
            score += ord(item) -96

        break


print(score)
